var controls, camera, scene, webGLRenderer, css3DRenderer, light, mesh, domElement;
init();
animate();

function init() {
    // Create the scene
    scene = new THREE.Scene();

    // Create the camera
    camera = createCamera();

    // Create the light
    light = createLight();
    scene.add( light );

    // Create the cube mesh
    mesh = createOctahedron();
    scene.add( mesh );

    // Create the renderers
    webGLRenderer = createWebGLRenderer();
    // css3DRenderer = createCSS3DRenderer();

    // Create the controls
    controls = createControls();

    // Create the event listener for on resize
    window.addEventListener( 'resize', onWindowResize, false );
    onWindowResize();
}

function createCamera() {
    var camera = new THREE.PerspectiveCamera( 45, window.innerWidth/window.innerHeight, 1, 1000 );
    camera.position.z = 0;
    camera.position.x = 500;
    return camera;
}

function createLight() {
    var lights = new THREE.Group();

    var directionalLight = new THREE.DirectionalLight( 0xffffff, 1 );
    directionalLight.position = new THREE.Vector3(  300, 0, 300 );
    directionalLight.lookAt( new THREE.Vector3( 0, 0, 0 ) );
    lights.add( directionalLight );

    var ambientLight = new THREE.AmbientLight( 0xffffff, .5 );
    ambientLight.position = new THREE.Vector3(  0, -200, 400 );
    ambientLight.lookAt( new THREE.Vector3( 0, 0, 0 ) );
    lights.add( ambientLight );

    return lights;
}

function createOctahedron() {
    var geometry = new THREE.OctahedronGeometry(200.0, 0);
    var material = new THREE.MeshPhongMaterial( {
        color: 0x002d77,
        specular: 0x111111,
        shininess: 30,
    } );
    var octahedron = new THREE.Mesh(geometry, material);
    octahedron.position.set(0, 200, 0);
    octahedron.name = 'octahedron';
    return octahedron;
}

function createSphere() {
    var geometry = new THREE.SphereGeometry(200.0);
    var material = new THREE.MeshPhongMaterial( {
        color: 0x002d77,
        specular: 0x111111,
        shininess: 30,
    } );
    var sphere = new THREE.Mesh(geometry, material);
    sphere.position.set(0, 0, 0);
    sphere.name = 'sphere';
    return sphere;
}

function createWebGLRenderer() {
    var rendererContainer = document.querySelector("#webgl");
    var renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio( window.devicePixelRatio );
    rendererContainer.prepend( renderer.domElement );
    renderer.setSize( window.innerWidth, window.innerHeight );
    return renderer;
}

function createControls() {
    var controls = new THREE.OrbitControls( camera, webGLRenderer.domElement );
    return controls;
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    webGLRenderer.setSize( window.innerWidth, window.innerHeight );
}

function animate() {
    requestAnimationFrame( animate );
    controls.update();
    if (mesh.name === 'octahedron') {
        mesh.position.y -= 2;
        if (mesh.position.y === 0) {
            scene.remove(mesh);
            mesh = createSphere();
            scene.add(mesh);
        }
    } else if (mesh.name === 'sphere') {
        mesh.position.y += 2;
        if (mesh.position.y === 200) {
            scene.remove(mesh);
            mesh = createOctahedron();
            scene.add(mesh);
        }
    }
    webGLRenderer.render( scene, camera );
}
